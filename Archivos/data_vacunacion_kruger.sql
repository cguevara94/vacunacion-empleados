INSERT INTO public.estado_vacunacion(
	eliminado, nombre)
	VALUES ('N', 'Vacunado');
INSERT INTO public.estado_vacunacion(
	eliminado, nombre)
	VALUES ('N', 'No Vacunado');

INSERT INTO public.rol(
	eliminado, descripcion, nombre)
	VALUES ('N', 'Empleado de la empresa', 'Empleado');
	
INSERT INTO public.rol(
	eliminado, descripcion, nombre)
	VALUES ('N', 'Administrador de la aplicacion', 'Administrador');

INSERT INTO public.vacuna(
	eliminado, dosis_req, nombre)
	VALUES ('N', 2, 'Sputnik');
	
INSERT INTO public.vacuna(
	eliminado, dosis_req, nombre)
	VALUES ('N', 2, 'AstraZeneca');
	
INSERT INTO public.vacuna(
	eliminado, dosis_req, nombre)
	VALUES ('N', 2, 'Pfizer');
	
INSERT INTO public.vacuna(
	eliminado, dosis_req, nombre)
	VALUES ('N', 1, 'Jhonson&Jhonson');


INSERT INTO public.servicio(
	eliminado, parametros, path, verbo)
	VALUES ('N', null, '/empleados', 'GET');
	
INSERT INTO public.servicio(
	eliminado, parametros, path, verbo)
	VALUES ('N', 'fechaDesde,fechaHasta', '/empleados', 'GET');
	
INSERT INTO public.servicio(
	eliminado, parametros, path, verbo)
	VALUES ('N', 'tipoVacuna', '/empleados', 'GET');
	
INSERT INTO public.servicio(
	eliminado, parametros, path, verbo)
	VALUES ('N', 'estadoVacunacion', '/empleados', 'GET');
	
INSERT INTO public.servicio(
	eliminado, parametros, path, verbo)
	VALUES ('N', null, '/empleados/id', 'GET');
	
INSERT INTO public.servicio(
	eliminado, parametros, path, verbo)
	VALUES ('N', null, '/empleados/id', 'DELETE');
	
INSERT INTO public.servicio(
	eliminado, parametros, path, verbo)
	VALUES ('N', null, '/empleados', 'POST');
	
INSERT INTO public.servicio(
	eliminado, parametros, path, verbo)
	VALUES ('N', null, '/empleados/id', 'PUT');
	
INSERT INTO public.servicio(
	eliminado, parametros, path, verbo)
	VALUES ('N', null, '/empleados/informacion-adicional/id', 'PUT');



INSERT INTO public.servicio_rol(
	eliminado, id_rol, id_servicio)
	VALUES ('N', '2', '1');
	
INSERT INTO public.servicio_rol(
	eliminado, id_rol, id_servicio)
	VALUES ('N', '2', '2');
	
INSERT INTO public.servicio_rol(
	eliminado, id_rol, id_servicio)
	VALUES ('N', '2', '3');
	
INSERT INTO public.servicio_rol(
	eliminado, id_rol, id_servicio)
	VALUES ('N', '2', '4');
	
INSERT INTO public.servicio_rol(
	eliminado, id_rol, id_servicio)
	VALUES ('N', '2', '5');
	
INSERT INTO public.servicio_rol(
	eliminado, id_rol, id_servicio)
	VALUES ('N', '1', '5');
	
INSERT INTO public.servicio_rol(
	eliminado, id_rol, id_servicio)
	VALUES ('N', '2', '6');
	
INSERT INTO public.servicio_rol(
	eliminado, id_rol, id_servicio)
	VALUES ('N', '2', '7');

INSERT INTO public.servicio_rol(
	eliminado, id_rol, id_servicio)
	VALUES ('N', '2', '8');
	
INSERT INTO public.servicio_rol(
	eliminado, id_rol, id_servicio)
	VALUES ('N', '1', '9');





INSERT INTO public.usuario(
	eliminado, clave, nombre, id_empleado)
	VALUES ('N', '$2a$12$jYY7h0ZvZwJ6NczgtpgdqeuW04oS5cAmcqgvYT0NYp2sOmey9Frua', 'admin', null);



INSERT INTO public.usuario_rol(
	eliminado, id_rol, id_usuario)
	VALUES ('N', '2', '1');