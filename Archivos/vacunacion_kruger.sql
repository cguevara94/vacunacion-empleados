--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 14.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: empleado; Type: TABLE; Schema: public; Owner: dbadmin
--

CREATE TABLE public.empleado (
    id_empleado bigint NOT NULL,
    eliminado character(1) NOT NULL,
    apellidos character varying(50) NOT NULL,
    cedula character varying(10) NOT NULL,
    direccion_domicilio character varying(255),
    email character varying(50) NOT NULL,
    fecha_nacimiento date,
    nombres character varying(50) NOT NULL,
    movil character varying(10),
    id_estado_vac bigint,
    CONSTRAINT empleado_eliminado_check CHECK ((eliminado = ANY (ARRAY['S'::bpchar, 'N'::bpchar])))
);


ALTER TABLE public.empleado OWNER TO dbadmin;

--
-- Name: empleado_id_empleado_seq; Type: SEQUENCE; Schema: public; Owner: dbadmin
--

CREATE SEQUENCE public.empleado_id_empleado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.empleado_id_empleado_seq OWNER TO dbadmin;

--
-- Name: empleado_id_empleado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dbadmin
--

ALTER SEQUENCE public.empleado_id_empleado_seq OWNED BY public.empleado.id_empleado;


--
-- Name: estado_vacunacion; Type: TABLE; Schema: public; Owner: dbadmin
--

CREATE TABLE public.estado_vacunacion (
    id_estado_vacunacion bigint NOT NULL,
    eliminado character(1) NOT NULL,
    nombre character varying(30) NOT NULL,
    CONSTRAINT estado_vacunacion_eliminado_check CHECK ((eliminado = ANY (ARRAY['S'::bpchar, 'N'::bpchar])))
);


ALTER TABLE public.estado_vacunacion OWNER TO dbadmin;

--
-- Name: estado_vacunacion_id_estado_vacunacion_seq; Type: SEQUENCE; Schema: public; Owner: dbadmin
--

CREATE SEQUENCE public.estado_vacunacion_id_estado_vacunacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estado_vacunacion_id_estado_vacunacion_seq OWNER TO dbadmin;

--
-- Name: estado_vacunacion_id_estado_vacunacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dbadmin
--

ALTER SEQUENCE public.estado_vacunacion_id_estado_vacunacion_seq OWNED BY public.estado_vacunacion.id_estado_vacunacion;


--
-- Name: rol; Type: TABLE; Schema: public; Owner: dbadmin
--

CREATE TABLE public.rol (
    id_rol bigint NOT NULL,
    eliminado character(1) NOT NULL,
    descripcion character varying(150),
    nombre character varying(50) NOT NULL,
    CONSTRAINT rol_eliminado_check CHECK ((eliminado = ANY (ARRAY['S'::bpchar, 'N'::bpchar])))
);


ALTER TABLE public.rol OWNER TO dbadmin;

--
-- Name: rol_id_rol_seq; Type: SEQUENCE; Schema: public; Owner: dbadmin
--

CREATE SEQUENCE public.rol_id_rol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rol_id_rol_seq OWNER TO dbadmin;

--
-- Name: rol_id_rol_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dbadmin
--

ALTER SEQUENCE public.rol_id_rol_seq OWNED BY public.rol.id_rol;


--
-- Name: servicio; Type: TABLE; Schema: public; Owner: dbadmin
--

CREATE TABLE public.servicio (
    id_servicio bigint NOT NULL,
    eliminado character(1) NOT NULL,
    parametros character varying(255),
    path character varying(255) NOT NULL,
    verbo character varying(255),
    CONSTRAINT servicio_eliminado_check CHECK ((eliminado = ANY (ARRAY['S'::bpchar, 'N'::bpchar])))
);


ALTER TABLE public.servicio OWNER TO dbadmin;

--
-- Name: servicio_id_servicio_seq; Type: SEQUENCE; Schema: public; Owner: dbadmin
--

CREATE SEQUENCE public.servicio_id_servicio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.servicio_id_servicio_seq OWNER TO dbadmin;

--
-- Name: servicio_id_servicio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dbadmin
--

ALTER SEQUENCE public.servicio_id_servicio_seq OWNED BY public.servicio.id_servicio;


--
-- Name: servicio_rol; Type: TABLE; Schema: public; Owner: dbadmin
--

CREATE TABLE public.servicio_rol (
    id_servicio_rol bigint NOT NULL,
    eliminado character(1) NOT NULL,
    id_rol bigint NOT NULL,
    id_servicio bigint NOT NULL,
    CONSTRAINT servicio_rol_eliminado_check CHECK ((eliminado = ANY (ARRAY['S'::bpchar, 'N'::bpchar])))
);


ALTER TABLE public.servicio_rol OWNER TO dbadmin;

--
-- Name: servicio_rol_id_servicio_rol_seq; Type: SEQUENCE; Schema: public; Owner: dbadmin
--

CREATE SEQUENCE public.servicio_rol_id_servicio_rol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.servicio_rol_id_servicio_rol_seq OWNER TO dbadmin;

--
-- Name: servicio_rol_id_servicio_rol_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dbadmin
--

ALTER SEQUENCE public.servicio_rol_id_servicio_rol_seq OWNED BY public.servicio_rol.id_servicio_rol;


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: dbadmin
--

CREATE TABLE public.usuario (
    id_usuario bigint NOT NULL,
    eliminado character(1) NOT NULL,
    clave character varying(255) NOT NULL,
    nombre character varying(50) NOT NULL,
    id_empleado bigint,
    CONSTRAINT usuario_eliminado_check CHECK ((eliminado = ANY (ARRAY['S'::bpchar, 'N'::bpchar])))
);


ALTER TABLE public.usuario OWNER TO dbadmin;

--
-- Name: usuario_id_usuario_seq; Type: SEQUENCE; Schema: public; Owner: dbadmin
--

CREATE SEQUENCE public.usuario_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_usuario_seq OWNER TO dbadmin;

--
-- Name: usuario_id_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dbadmin
--

ALTER SEQUENCE public.usuario_id_usuario_seq OWNED BY public.usuario.id_usuario;


--
-- Name: usuario_rol; Type: TABLE; Schema: public; Owner: dbadmin
--

CREATE TABLE public.usuario_rol (
    id_usuario_rol bigint NOT NULL,
    eliminado character(1) NOT NULL,
    id_rol bigint NOT NULL,
    id_usuario bigint NOT NULL,
    CONSTRAINT usuario_rol_eliminado_check CHECK ((eliminado = ANY (ARRAY['S'::bpchar, 'N'::bpchar])))
);


ALTER TABLE public.usuario_rol OWNER TO dbadmin;

--
-- Name: usuario_rol_id_usuario_rol_seq; Type: SEQUENCE; Schema: public; Owner: dbadmin
--

CREATE SEQUENCE public.usuario_rol_id_usuario_rol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_rol_id_usuario_rol_seq OWNER TO dbadmin;

--
-- Name: usuario_rol_id_usuario_rol_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dbadmin
--

ALTER SEQUENCE public.usuario_rol_id_usuario_rol_seq OWNED BY public.usuario_rol.id_usuario_rol;


--
-- Name: vacuna; Type: TABLE; Schema: public; Owner: dbadmin
--

CREATE TABLE public.vacuna (
    id_vacuna bigint NOT NULL,
    eliminado character(1) NOT NULL,
    dosis_req integer NOT NULL,
    nombre character varying(50) NOT NULL,
    CONSTRAINT vacuna_eliminado_check CHECK ((eliminado = ANY (ARRAY['S'::bpchar, 'N'::bpchar])))
);


ALTER TABLE public.vacuna OWNER TO dbadmin;

--
-- Name: vacuna_id_vacuna_seq; Type: SEQUENCE; Schema: public; Owner: dbadmin
--

CREATE SEQUENCE public.vacuna_id_vacuna_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vacuna_id_vacuna_seq OWNER TO dbadmin;

--
-- Name: vacuna_id_vacuna_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dbadmin
--

ALTER SEQUENCE public.vacuna_id_vacuna_seq OWNED BY public.vacuna.id_vacuna;


--
-- Name: vacunacion_empleado; Type: TABLE; Schema: public; Owner: dbadmin
--

CREATE TABLE public.vacunacion_empleado (
    id_vacunacion_empleado bigint NOT NULL,
    eliminado character(1) NOT NULL,
    dosis_recibidas integer NOT NULL,
    fecha_ult_dosis date NOT NULL,
    id_empleado bigint NOT NULL,
    id_vacuna bigint NOT NULL,
    CONSTRAINT vacunacion_empleado_eliminado_check CHECK ((eliminado = ANY (ARRAY['S'::bpchar, 'N'::bpchar])))
);


ALTER TABLE public.vacunacion_empleado OWNER TO dbadmin;

--
-- Name: vacunacion_empleado_id_vacunacion_empleado_seq; Type: SEQUENCE; Schema: public; Owner: dbadmin
--

CREATE SEQUENCE public.vacunacion_empleado_id_vacunacion_empleado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vacunacion_empleado_id_vacunacion_empleado_seq OWNER TO dbadmin;

--
-- Name: vacunacion_empleado_id_vacunacion_empleado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dbadmin
--

ALTER SEQUENCE public.vacunacion_empleado_id_vacunacion_empleado_seq OWNED BY public.vacunacion_empleado.id_vacunacion_empleado;


--
-- Name: empleado id_empleado; Type: DEFAULT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.empleado ALTER COLUMN id_empleado SET DEFAULT nextval('public.empleado_id_empleado_seq'::regclass);


--
-- Name: estado_vacunacion id_estado_vacunacion; Type: DEFAULT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.estado_vacunacion ALTER COLUMN id_estado_vacunacion SET DEFAULT nextval('public.estado_vacunacion_id_estado_vacunacion_seq'::regclass);


--
-- Name: rol id_rol; Type: DEFAULT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.rol ALTER COLUMN id_rol SET DEFAULT nextval('public.rol_id_rol_seq'::regclass);


--
-- Name: servicio id_servicio; Type: DEFAULT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.servicio ALTER COLUMN id_servicio SET DEFAULT nextval('public.servicio_id_servicio_seq'::regclass);


--
-- Name: servicio_rol id_servicio_rol; Type: DEFAULT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.servicio_rol ALTER COLUMN id_servicio_rol SET DEFAULT nextval('public.servicio_rol_id_servicio_rol_seq'::regclass);


--
-- Name: usuario id_usuario; Type: DEFAULT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.usuario ALTER COLUMN id_usuario SET DEFAULT nextval('public.usuario_id_usuario_seq'::regclass);


--
-- Name: usuario_rol id_usuario_rol; Type: DEFAULT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.usuario_rol ALTER COLUMN id_usuario_rol SET DEFAULT nextval('public.usuario_rol_id_usuario_rol_seq'::regclass);


--
-- Name: vacuna id_vacuna; Type: DEFAULT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.vacuna ALTER COLUMN id_vacuna SET DEFAULT nextval('public.vacuna_id_vacuna_seq'::regclass);


--
-- Name: vacunacion_empleado id_vacunacion_empleado; Type: DEFAULT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.vacunacion_empleado ALTER COLUMN id_vacunacion_empleado SET DEFAULT nextval('public.vacunacion_empleado_id_vacunacion_empleado_seq'::regclass);

--
-- Name: empleado empleado_pkey; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.empleado
    ADD CONSTRAINT empleado_pkey PRIMARY KEY (id_empleado);


--
-- Name: estado_vacunacion estado_vacunacion_pkey; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.estado_vacunacion
    ADD CONSTRAINT estado_vacunacion_pkey PRIMARY KEY (id_estado_vacunacion);


--
-- Name: rol rol_pkey; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (id_rol);


--
-- Name: servicio servicio_pkey; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.servicio
    ADD CONSTRAINT servicio_pkey PRIMARY KEY (id_servicio);


--
-- Name: servicio_rol servicio_rol_pkey; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.servicio_rol
    ADD CONSTRAINT servicio_rol_pkey PRIMARY KEY (id_servicio_rol);


--
-- Name: empleado unq_cedula; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.empleado
    ADD CONSTRAINT unq_cedula UNIQUE (cedula);


--
-- Name: usuario unq_empleado; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT unq_empleado UNIQUE (id_empleado);


--
-- Name: estado_vacunacion unq_nombre; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.estado_vacunacion
    ADD CONSTRAINT unq_nombre UNIQUE (nombre);


--
-- Name: vacuna unq_nombre_vacuna; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.vacuna
    ADD CONSTRAINT unq_nombre_vacuna UNIQUE (nombre);


--
-- Name: usuario_rol unq_usuario_rol; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.usuario_rol
    ADD CONSTRAINT unq_usuario_rol UNIQUE (id_usuario, id_rol);


--
-- Name: vacunacion_empleado unq_vacunacion_emp; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.vacunacion_empleado
    ADD CONSTRAINT unq_vacunacion_emp UNIQUE (id_empleado, id_vacuna);


--
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id_usuario);


--
-- Name: usuario_rol usuario_rol_pkey; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.usuario_rol
    ADD CONSTRAINT usuario_rol_pkey PRIMARY KEY (id_usuario_rol);


--
-- Name: vacuna vacuna_pkey; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.vacuna
    ADD CONSTRAINT vacuna_pkey PRIMARY KEY (id_vacuna);


--
-- Name: vacunacion_empleado vacunacion_empleado_pkey; Type: CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.vacunacion_empleado
    ADD CONSTRAINT vacunacion_empleado_pkey PRIMARY KEY (id_vacunacion_empleado);


--
-- Name: empleado fk_emp_estado_vac; Type: FK CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.empleado
    ADD CONSTRAINT fk_emp_estado_vac FOREIGN KEY (id_estado_vac) REFERENCES public.estado_vacunacion(id_estado_vacunacion);


--
-- Name: usuario fk_empleado; Type: FK CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT fk_empleado FOREIGN KEY (id_empleado) REFERENCES public.empleado(id_empleado);


--
-- Name: servicio_rol fk_rol; Type: FK CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.servicio_rol
    ADD CONSTRAINT fk_rol FOREIGN KEY (id_rol) REFERENCES public.rol(id_rol);


--
-- Name: usuario_rol fk_rol; Type: FK CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.usuario_rol
    ADD CONSTRAINT fk_rol FOREIGN KEY (id_rol) REFERENCES public.rol(id_rol);


--
-- Name: servicio_rol fk_servicio; Type: FK CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.servicio_rol
    ADD CONSTRAINT fk_servicio FOREIGN KEY (id_servicio) REFERENCES public.servicio(id_servicio);


--
-- Name: usuario_rol fk_usuario; Type: FK CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.usuario_rol
    ADD CONSTRAINT fk_usuario FOREIGN KEY (id_usuario) REFERENCES public.usuario(id_usuario);


--
-- Name: vacunacion_empleado fk_vac_emp; Type: FK CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.vacunacion_empleado
    ADD CONSTRAINT fk_vac_emp FOREIGN KEY (id_empleado) REFERENCES public.empleado(id_empleado);


--
-- Name: vacunacion_empleado fk_vac_vacuna; Type: FK CONSTRAINT; Schema: public; Owner: dbadmin
--

ALTER TABLE ONLY public.vacunacion_empleado
    ADD CONSTRAINT fk_vac_vacuna FOREIGN KEY (id_vacuna) REFERENCES public.vacuna(id_vacuna);


--
-- PostgreSQL database dump complete
--


