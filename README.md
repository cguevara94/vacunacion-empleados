# README #

### Repositorio de la aplicación vacunacion-backend ###

* Aplicación para registro de vacunación de los empleados de la empresa Kruger
* Versión: 0.0.1-SNAPSHOT

### Configuración, construcción y despliegue ###

###### Configuración base de datos ######

Los archivos se encuentra dentro de la carpeta archivos de la raíz del repositorio.

* Crear la base de datos: vacunacion_kruger
* Ejecutar el archivo: vacunacion_kruger.sql
* Ejecutar el archivo: data_vacunacion_kruger.sql

###### Configuración del servidor ######

Reemplazar JBOSS_HOME por el directorio raíz del servidor y PATH_ARCHIVO por la ruta de la carpeta contenedora del archivo.

* Descargar desde https://download.jboss.org/wildfly/20.0.1.Final/wildfly-20.0.1.Final.zip
* Levantar el servidor ejecutando en la terminal:
	* Windows -> JBOSS_HOME\bin\standalone.bat --server-config=standalone-full.xml -Djboss.server.base.dir=JBOSS_HOME\standalone -b 0.0.0.0
	* LINUX/MAC -> JBOSS_HOME/bin/standalone.sh --server-config=standalone-full.xml -Djboss.server.base.dir=JBOSS_HOME/standalone -b 0.0.0.0
* Remover deployment scanner
	* Ejecutar el archivo configuraciones_generales.cli (se encuentra dentro de la carpeta archivos de la raíz del repositorio) usando la terminal: 
		* WINDOWS: JBOSS_HOME\bin\jboss-cli.bat -c --controller=localhost:9990 --file=PATH_ARCHIVO\configuraciones_generales.cli
		* LINUX/MAC: JBOSS_HOME/bin/jboss-cli.sh -c --controller=localhost:9990 --file=PATH_ARCHIVO/configuraciones_generales.cli

###### Construcción del artefacto ######
* Clonar usando en la terminal el comando: git clone https://cguevara94@bitbucket.org/cguevara94/vacunacion-empleados.git
* Actualizar spring.datasource.username, spring.datasource.password y spring.datasource.url con los valores correspondientes al ambiente.
* Abrir la terminal y moverse a la carpeta del proyecto
* Ejecutar el comando: mvn clean install
* Copiar el war vacunacion-backend-0.0.1-SNAPSHOT generado en la carpeta target en la carpeta deployment del servidor
* Desplegar el artefacto:
	* Ingresar a cli en la terminal: 
		* WINDOWS: JBOSS_HOME\bin\jboss-cli.bat -c --controller=localhost:9990
		* LINUX/MAC: JBOSS_HOME/bin/jboss-cli.sh -c --controller=localhost:9990
	* Ejecutar: 
		* WINDOWS: deploy JBOSS_HOME\standalone\deployments\vacunacion-backend-0.0.1-SNAPSHOT.war --runtime-name=vacunacion-backend.war --unmanaged
		* LINUX/MAC: deploy JBOSS_HOME/standalone/deployments/vacunacion-backend-0.0.1-SNAPSHOT.war --runtime-name=vacunacion-backend.war --unmanaged

###### Url contexto aplicación ######

	* http://localhost:8080/vacunacion-backend/

###### Documentación ######

	* http://localhost:8080/vacunacion-backend/api/doc/ui
