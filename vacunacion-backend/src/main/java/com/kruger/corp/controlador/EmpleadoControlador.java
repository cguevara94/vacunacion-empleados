/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kruger.corp.controlador;

import com.kruger.corp.excepcion.ActualizaRegistroExcepcion;
import com.kruger.corp.excepcion.InformacionNoValidaExcepcion;
import com.kruger.corp.excepcion.RegistroNoEncontradoExcepcion;
import com.kruger.corp.modelo.Empleado;
import com.kruger.corp.servicio.IEmpleadoServicio;
import com.kruger.corp.validacion.empleado.InformacionAdicional;
import com.kruger.corp.validacion.empleado.InformacionBasica;
import java.net.URI;
import java.time.LocalDate;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author cguevara94
 */
@RestController
@RequestMapping("/empleados")
public class EmpleadoControlador {

    @Autowired
    private IEmpleadoServicio empleadoServicio;

    @PreAuthorize("@authServiceImpl.tieneAcceso('/empleados', #request)")
    @GetMapping
    public ResponseEntity<Page<Empleado>> listar(@RequestParam(name = "estadoVacunacion", required = false) String estadoVacunacion,
            @RequestParam(name = "tipoVacuna", required = false) String tipoVacuna,
            @RequestParam(name = "fechaDesde", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate fechaDesde,
            @RequestParam(name = "fechaHasta", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate fechaHasta,
            Pageable pageable, HttpServletRequest request) {
        Page<Empleado> empleados = null;
        if (Objects.nonNull(estadoVacunacion)) {
            empleados = empleadoServicio.buscarPorEstadoVacunacion(estadoVacunacion, pageable);
        }

        if (Objects.nonNull(tipoVacuna)) {
            empleados = empleadoServicio.buscarPorVacuna(tipoVacuna, pageable);
        }

        if (Objects.nonNull(fechaDesde) && Objects.nonNull(fechaHasta)) {
            empleados = empleadoServicio.buscarPorRangoFechaVacunacion(fechaDesde, fechaHasta, pageable);
        }

        if (Objects.isNull(empleados)) {
            empleados = empleadoServicio.buscarTodos(pageable);
        }

        return new ResponseEntity<>(empleados, HttpStatus.OK);
    }

    @PreAuthorize("@authServiceImpl.tieneAcceso('/empleados/id', #request)")
    @GetMapping("/{id}")
    public ResponseEntity<Empleado> listarPorId(@PathVariable("id") Long id, HttpServletRequest request) {
        Empleado empleado = empleadoServicio.buscarPorId(id);

        return new ResponseEntity<>(empleado, HttpStatus.OK);
    }

    @PreAuthorize("@authServiceImpl.tieneAcceso('/empleados/id', #request)")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminar(@PathVariable("id") Long id, HttpServletRequest request) throws RegistroNoEncontradoExcepcion {
        empleadoServicio.eliminar(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("@authServiceImpl.tieneAcceso('/empleados', #request)")
    @PostMapping
    public ResponseEntity<Empleado> registrar(@Validated(InformacionBasica.class) @RequestBody Empleado empleado, HttpServletRequest request) {
        Empleado empleadoRegistrado = empleadoServicio.registrarInformacionBasica(empleado);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(empleadoRegistrado.getId()).toUri();

        return ResponseEntity.created(location).body(empleadoRegistrado);
    }

    @PreAuthorize("@authServiceImpl.tieneAcceso('/empleados/id', #request)")
    @PutMapping("/{id}")
    public ResponseEntity<Empleado> modificar(@PathVariable("id") Long id,
            @Validated(InformacionBasica.class) @RequestBody Empleado empleado, HttpServletRequest request) throws ActualizaRegistroExcepcion {
        empleado.setId(id);
        Empleado empleadoActualizado = empleadoServicio.actualizarInformacionBasica(empleado);
        return new ResponseEntity<>(empleadoActualizado, HttpStatus.OK);
    }

    @PreAuthorize("@authServiceImpl.tieneAcceso('/empleados/informacion-adicional/id', #request)")
    @PutMapping("/informacion-adicional/{id}")
    public ResponseEntity<Empleado> modificarDatosAdicionales(@PathVariable("id") Long id,
            @Validated(InformacionAdicional.class) @RequestBody Empleado empleado, HttpServletRequest request) throws InformacionNoValidaExcepcion {
        empleado.setId(id);
        Empleado empleadoActualizado = empleadoServicio.actualizarInformacionAdicional(empleado);
        return new ResponseEntity<>(empleadoActualizado, HttpStatus.OK);
    }

}
