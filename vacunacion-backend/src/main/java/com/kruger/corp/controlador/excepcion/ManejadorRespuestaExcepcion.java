package com.kruger.corp.controlador.excepcion;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.kruger.corp.excepcion.InformacionNoValidaExcepcion;
import com.kruger.corp.excepcion.RegistroNoEncontradoExcepcion;

@ControllerAdvice
@RestController
public class ManejadorRespuestaExcepcion extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<RespuestaExcepcion> manejarTodasExcepciones(Exception ex, WebRequest request){		
		RespuestaExcepcion er = 
				new RespuestaExcepcion(LocalDateTime.now(), ex.getMessage(), 
						request.getDescription(false), HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.name());		
		return new ResponseEntity<RespuestaExcepcion>(er, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		String mensaje = ex.getBindingResult().getAllErrors().stream().map(e -> {
			return e.getDefaultMessage().toString().concat(", ");
		}).collect(Collectors.joining());

		RespuestaExcepcion er = new RespuestaExcepcion(LocalDateTime.now(), mensaje,
				request.getDescription(false), HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name());

		return new ResponseEntity<Object>(er, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({RegistroNoEncontradoExcepcion.class})
	public ResponseEntity<RespuestaExcepcion> manejarRegistroNoEncontradoExcepcion(Exception ex,
			WebRequest request) {
		
		RespuestaExcepcion er = new RespuestaExcepcion(LocalDateTime.now(), ex.getMessage(),
				request.getDescription(false), HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.name());
		return new ResponseEntity<RespuestaExcepcion>(er, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler({InformacionNoValidaExcepcion.class})
	public ResponseEntity<RespuestaExcepcion> manejarInformacionNoValidaExcepcion(Exception ex,
			WebRequest request) {
		
		RespuestaExcepcion er = new RespuestaExcepcion(LocalDateTime.now(), ex.getMessage(),
				request.getDescription(false), HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name());
		return new ResponseEntity<RespuestaExcepcion>(er, HttpStatus.BAD_REQUEST);
	}
	
}
