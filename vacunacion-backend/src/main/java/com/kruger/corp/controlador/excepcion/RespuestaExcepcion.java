package com.kruger.corp.controlador.excepcion;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RespuestaExcepcion {
	
	private LocalDateTime fecha;
	private String mensaje;
	private String detalles;
	private int codigoEstado;
	private String estado;

}
