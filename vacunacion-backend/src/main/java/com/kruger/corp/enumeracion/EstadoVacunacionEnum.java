package com.kruger.corp.enumeracion;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author cguevara94
 */
@AllArgsConstructor
@Getter
public enum EstadoVacunacionEnum {

    VACUNADO("Vacunado"), NO_VACUNADO("No Vacunado");

    private final String descripcion;

}
