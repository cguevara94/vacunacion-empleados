package com.kruger.corp.enumeracion;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author cguevara94
 */
@AllArgsConstructor
@Getter
public enum RolEnum {

    EMPLEADO("Empleado"), ADMINISTADOR("Administador");

    private final String nombreRol;

}
