package com.kruger.corp.enumeracion;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author cguevara94
 */
@AllArgsConstructor
@Getter
public enum SiNoEnum {
    
    S("Si"), N("No");
    
    private final String siNoValor;
    
}
