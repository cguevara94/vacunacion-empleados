package com.kruger.corp.excepcion;

/**
 *
 * @author cguevara94
 */
public class ActualizaRegistroExcepcion extends Exception {

    public ActualizaRegistroExcepcion(String message) {
        super(message);
    }

}
