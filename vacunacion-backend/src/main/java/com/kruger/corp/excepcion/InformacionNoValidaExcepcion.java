package com.kruger.corp.excepcion;

/**
 *
 * @author cguevara94
 */
public class InformacionNoValidaExcepcion extends Exception {

    public InformacionNoValidaExcepcion(String message) {
        super(message);
    }

}
