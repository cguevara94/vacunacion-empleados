package com.kruger.corp.excepcion;

/**
 *
 * @author cguevara94
 */
public class RegistroNoEncontradoExcepcion extends Exception {

    public RegistroNoEncontradoExcepcion(String message) {
        super(message);
    }

}
