package com.kruger.corp.modelo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kruger.corp.enumeracion.EstadoVacunacionEnum;
import com.kruger.corp.validacion.Cedula;
import com.kruger.corp.validacion.empleado.InformacionAdicional;
import com.kruger.corp.validacion.empleado.InformacionBasica;
import com.kruger.corp.validacion.empleado.InformacionVacunacion;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Check;

/**
 *
 * @author cguevara94
 */
@Entity
@Table(name = "empleado",
        uniqueConstraints = {
            @UniqueConstraint(name = "unq_cedula", columnNames = {"cedula"})
        }
)
@Check(constraints = "eliminado IN ('S', 'N')")
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Empleado extends EntidadBase implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7504494521585604958L;

	@Id
    @Column(name = "id_empleado")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "{cedula.vacio}", groups = {InformacionBasica.class})
    @Cedula(message = "{cedula.validacion}", groups = {InformacionBasica.class})
    @Column(name = "cedula", nullable = false, length = 10)
    private String cedula;

    @NotBlank(message = "{nombres.vacio}", groups = {InformacionBasica.class})
    @Size(min = 2, max = 50, message = "{nombres.tamanio}", groups = {InformacionBasica.class})
    @Pattern(regexp = "^[A-Za-z]+[' ']{0,1}+[A-Za-z]+$", message = "{nombres.regex}", groups = {InformacionBasica.class})
    @Column(name = "nombres", nullable = false, length = 50)
    private String nombres;

    @NotBlank(message = "{apellidos.vacio}", groups = {InformacionBasica.class})
    @Size(min = 2, max = 50, message = "{apellidos.tamanio}", groups = {InformacionBasica.class})
    @Pattern(regexp = "^[A-Za-z]+[' ']{0,1}+[A-Za-z]+$", message = "{apellidos.regex}", groups = {InformacionBasica.class})
    @Column(name = "apellidos", nullable = false, length = 50)
    private String apellidos;

    @NotBlank(message = "{email.vacio}", groups = {InformacionBasica.class})
    @Email(message = "{email.invalido}", groups = {InformacionBasica.class})
    @Size(max = 50, message = "email.tamanio", groups = {InformacionBasica.class})
    @Column(name = "email", nullable = false, length = 50)
    private String email;

    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;

    @Size(min = 3, max = 255, message = "{direccionDomicilio.tamanio}", groups = {InformacionAdicional.class})
    @Pattern(regexp = "^(?!\\\\s*$).+", message = "{direccionDomicilio.vacio}", groups = {InformacionAdicional.class})
    @Column(name = "direccion_domicilio")
    private String dirDomicilio;

    @Pattern(regexp = "^[0-9]{10,10}+$", message = "{movil.tipoDato}", groups = {InformacionAdicional.class})
    @Column(name = "movil", length = 10)
    private String numMovil;

    @ManyToOne
    @JoinColumn(name = "id_estado_vac", foreignKey = @ForeignKey(name = "fk_emp_estado_vac"))
    private EstadoVacunacion estadoVacunacion;

    @NotEmpty(message = "{datos.vacunacion.obligatorio}", groups = {InformacionVacunacion.class})
    @OneToMany(mappedBy = "empleado", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<VacunacionEmpleado> vacunacionEmpleado;

    public Empleado(String cedula, String nombres, String apellidos, String email) {
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.email = email;
    }

    public boolean tieneInfomacionAdicional() {
        return Objects.nonNull(fechaNacimiento) || Objects.nonNull(dirDomicilio)
                || Objects.nonNull(numMovil);
    }

    public boolean tieneInfomacionVacunacion() {
        return Objects.nonNull(estadoVacunacion)
                || (Objects.nonNull(vacunacionEmpleado) && !vacunacionEmpleado.isEmpty());
    }

    public String[] obtenerArrayNombres() {
        return getNombres().trim().split(" ");
    }

    public String[] obtenerArrayApellidos() {
        return getApellidos().trim().split(" ");
    }

    public boolean esVacunado() {
        return Objects.nonNull(estadoVacunacion)
                && EstadoVacunacionEnum.VACUNADO.getDescripcion().equals(estadoVacunacion.getNombre());
    }

}
