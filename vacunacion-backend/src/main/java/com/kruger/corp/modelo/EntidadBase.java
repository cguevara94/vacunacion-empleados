package com.kruger.corp.modelo;

import com.kruger.corp.enumeracion.SiNoEnum;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author cguevara94
 */
@MappedSuperclass
public class EntidadBase {

    @Column(name = "eliminado", nullable = false, columnDefinition = "char")
    @Enumerated(EnumType.STRING)
    protected SiNoEnum eliminado;

    public EntidadBase() {
        eliminado = SiNoEnum.N;
    }

    public void marcarEliminado() {
        eliminado = SiNoEnum.S;
    }

    public boolean esEliminado() {
        return SiNoEnum.S.equals(eliminado);
    }

}
