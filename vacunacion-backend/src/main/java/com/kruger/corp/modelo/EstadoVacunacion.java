package com.kruger.corp.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Check;

/**
 *
 * @author cguevara94
 */
@Entity
@Table(name = "estado_vacunacion",
        uniqueConstraints = {
            @UniqueConstraint(name = "unq_nombre", columnNames = {"nombre"})
        }
)
@Check(constraints = "eliminado IN ('S', 'N')")
@NoArgsConstructor
@Data
public class EstadoVacunacion extends EntidadBase implements Serializable {

    @Id
    @Column(name = "id_estado_vacunacion")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre", nullable = false, length = 30)
    private String nombre;

    public EstadoVacunacion(String nombre) {
        this.nombre = nombre;
    }

}
