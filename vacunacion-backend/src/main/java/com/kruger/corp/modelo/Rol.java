package com.kruger.corp.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Data;
import org.hibernate.annotations.Check;

/**
 *
 * @author cguevara94
 */
@Entity
@Table(name = "rol",
        uniqueConstraints = {
            @UniqueConstraint(name = "unq_nombre", columnNames = {"nombre"})
        }
)
@Check(constraints = "eliminado IN ('S', 'N')")
@Data
public class Rol extends EntidadBase implements Serializable {

    @Id
    @Column(name = "id_rol")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    @Column(name = "descripcion", length = 150)
    private String descripcion;

}
