package com.kruger.corp.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Check;

/**
 *
 * @author cguevara94
 */
@Entity
@Table(name = "servicio")
@Check(constraints = "eliminado IN ('S', 'N')")
@NoArgsConstructor
@Data
public class Servicio extends EntidadBase implements Serializable {

    @Id
    @Column(name = "id_servicio")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "path", nullable = false)
    private String path;

    @Column(name = "parametros")
    private String parametros;

    @Column(name = "verbo")
    private String verbo;

    @OneToMany(mappedBy = "servicio", fetch = FetchType.LAZY)
    private List<ServicioRol> roles;

}
