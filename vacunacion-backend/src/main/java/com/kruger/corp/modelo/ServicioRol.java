package com.kruger.corp.modelo;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Check;

/**
 *
 * @author cguevara94
 */
@Entity
@Table(name = "servicio_rol")
@Check(constraints = "eliminado IN ('S', 'N')")
@NoArgsConstructor
@Data
public class ServicioRol extends EntidadBase implements Serializable {

    @Id
    @Column(name = "id_servicio_rol")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_servicio", nullable = false, foreignKey = @ForeignKey(name = "fk_servicio"))
    private Servicio servicio;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_rol", nullable = false, foreignKey = @ForeignKey(name = "fk_rol"))
    private Rol rol;

}
