package com.kruger.corp.modelo;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Check;

/**
 *
 * @author cguevara94
 */
@Entity
@Table(name = "usuario",
        uniqueConstraints = {
            @UniqueConstraint(name = "unq_nombre", columnNames = {"nombre"}),
            @UniqueConstraint(name = "unq_empleado", columnNames = {"id_empleado"})
        }
)
@Check(constraints = "eliminado IN ('S', 'N')")
@NoArgsConstructor
@Data
public class Usuario extends EntidadBase implements Serializable {

    @Id
    @Column(name = "id_usuario")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    @Column(name = "clave", nullable = false, length = 255)
    private String clave;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_empleado", referencedColumnName = "id_empleado", foreignKey = @ForeignKey(name = "fk_empleado"))
    private Empleado empleado;

    @OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
    private List<UsuarioRol> roles;

    public Usuario(Empleado empleado) {
        this.nombre = generarNombreUsuarioEmpleado(empleado);
        this.clave = empleado.getCedula();
        this.empleado = empleado;
    }

    private String generarNombreUsuarioEmpleado(Empleado empleado) {
        StringBuilder builder = new StringBuilder();

        for (String nombreEmpleado : empleado.obtenerArrayNombres()) {
            if (nombreEmpleado.length() > 0) {
                builder.append(nombreEmpleado.charAt(0));
            }
        }

        String[] apellidosEmpleado = empleado.obtenerArrayApellidos();
        if (apellidosEmpleado.length > 0) {
            builder.append(apellidosEmpleado[0]);
        }

        LocalDate fechaCreacion = LocalDate.now();

        String anio = String.valueOf(fechaCreacion.getYear());
        anio = anio.substring(anio.length() - 2, anio.length());

        builder.append(String.format("%02d", fechaCreacion.getDayOfMonth()));
        builder.append(String.format("%02d", fechaCreacion.getMonthValue()));
        builder.append(anio);

        return builder.toString().toLowerCase();
    }

}
