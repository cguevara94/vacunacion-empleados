package com.kruger.corp.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Check;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author cguevara94
 */
@Entity
@Table(name = "vacuna",
        uniqueConstraints = {
            @UniqueConstraint(name = "unq_nombre_vacuna", columnNames = {"nombre"})
        }
)
@Check(constraints = "eliminado IN ('S', 'N')")
@NoArgsConstructor
@Data
public class Vacuna extends EntidadBase implements Serializable {

    @Id
    @Column(name = "id_vacuna")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    @Column(name = "dosis_req", nullable = false)
    private Integer dosisRequeridas;

    @JsonIgnore
    @OneToMany(mappedBy = "vacuna", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<VacunacionEmpleado> vacunacionEmpleado;

    public Vacuna(String nombre) {
        this.nombre = nombre;
    }

    public Vacuna(String nombre, Integer dosisRequeridas) {
        this.nombre = nombre;
        this.dosisRequeridas = dosisRequeridas;
    }

}
