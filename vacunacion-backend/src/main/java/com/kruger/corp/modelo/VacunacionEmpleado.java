package com.kruger.corp.modelo;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Check;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author cguevara94
 */
@Entity
@Table(name = "vacunacion_empleado",
        uniqueConstraints = {
            @UniqueConstraint(name = "unq_vacunacion_emp", columnNames = {"id_empleado", "id_vacuna"})
        }
)
@Check(constraints = "eliminado IN ('S', 'N')")
@NoArgsConstructor
@Data
public class VacunacionEmpleado extends EntidadBase implements Serializable {

    @Id
    @Column(name = "id_vacunacion_empleado")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "{fechaVacuna.obligatorio}")
    @Column(name = "fecha_ult_dosis", nullable = false)
    private LocalDate fechaUltimaDosis;

    @NotNull(message = "{numeroDosis.obligatorio}")
    @Column(name = "dosis_recibidas", nullable = false)
    private Integer dosisRecibidas;

    @JsonIgnore
    @NotNull(message = "{empleado.vacuna.obligatorio}")
    @ManyToOne
    @JoinColumn(name = "id_empleado", nullable = false, foreignKey = @ForeignKey(name = "fk_vac_emp"))
    private Empleado empleado;

    @NotNull(message = "{vacuna.obligatorio}")
    @ManyToOne
    @JoinColumn(name = "id_vacuna", nullable = false, foreignKey = @ForeignKey(name = "fk_vac_vacuna"))
    private Vacuna vacuna;

    public VacunacionEmpleado(LocalDate fechaUltimaDosis, Integer dosisRecibidas, Empleado empleado, Vacuna vacuna) {
        this.fechaUltimaDosis = fechaUltimaDosis;
        this.dosisRecibidas = dosisRecibidas;
        this.empleado = empleado;
        this.vacuna = vacuna;
    }

}
