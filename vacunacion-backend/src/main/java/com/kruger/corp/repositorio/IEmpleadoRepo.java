package com.kruger.corp.repositorio;

import com.kruger.corp.modelo.Empleado;
import java.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author cguevara94
 */
public interface IEmpleadoRepo extends IRepositorioGenerico<Empleado, Long> {

    @Modifying
    @Query("update Empleado e set e.fechaNacimiento = :fechaNacimiento, "
            + "e.dirDomicilio = :direccionDomicilio, e.numMovil = :movil, "
            + "e.estadoVacunacion.id = :idEstadoVacunacion where e.id = :idEmpleado")
    void actualizarInfoAdicional(@Param(value = "idEmpleado") Long idEmpleado,
            @Param(value = "fechaNacimiento") LocalDate fechaNacimiento,
            @Param(value = "direccionDomicilio") String domicilio, @Param(value = "movil") String movil,
            @Param(value = "idEstadoVacunacion") Long idEstadoVacunacion);

    @Query("FROM Empleado e JOIN e.estadoVacunacion ev WHERE e.eliminado='N' and ev.nombre=:estadoVacunacion")
    Page<Empleado> buscarPorEstadoVacunacion(@Param("estadoVacunacion") String estadoVacunacion, Pageable pageable);

    @Query("FROM Empleado e JOIN e.vacunacionEmpleado ve JOIN ve.vacuna v WHERE e.eliminado='N' and v.nombre=:nombreVacuna")
    Page<Empleado> buscarPorVacuna(@Param("nombreVacuna") String nombreVacuna, Pageable pageable);

    @Query("FROM Empleado e JOIN e.vacunacionEmpleado ve WHERE e.eliminado='N' and (ve.fechaUltimaDosis BETWEEN :fechaInicio AND :fechaFin)")
    Page<Empleado> buscarPorRangoFechaVacunacion(@Param("fechaInicio") LocalDate fechaInicio, @Param("fechaFin") LocalDate fechaFin, Pageable pageable);

    @Query("FROM Empleado e WHERE e.eliminado='N'")
    Page<Empleado> buscarTodos(Pageable pageable);

    @Query("FROM Empleado e WHERE e.eliminado='N' and e.id=:id")
    Empleado buscarPorId(@Param("id") Long id);

}
