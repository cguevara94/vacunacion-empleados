package com.kruger.corp.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 *
 * @author cguevara94
 */
@NoRepositoryBean
public interface IRepositorioGenerico <T, ID> extends JpaRepository<T, ID> {
    
}
