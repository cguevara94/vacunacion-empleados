package com.kruger.corp.repositorio;

import com.kruger.corp.modelo.Rol;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author cguevara94
 */
public interface IRolRepo extends IRepositorioGenerico<Rol, Long> {

    Rol findByNombre(@Param("nombre") String nombre);

}
