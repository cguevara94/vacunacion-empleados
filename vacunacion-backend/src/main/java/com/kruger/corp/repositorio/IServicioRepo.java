package com.kruger.corp.repositorio;

import com.kruger.corp.modelo.Servicio;
import com.kruger.corp.modelo.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author cguevara94
 */
public interface IServicioRepo extends IRepositorioGenerico<Usuario, Long> {

    @Query("FROM Servicio s JOIN FETCH s.roles rs JOIN FETCH rs.rol WHERE s.eliminado='N' and s.path=:path and s.parametros=:parametros and s.verbo=:verbo")
    Servicio buscarPorNombreConRoles(@Param("path") String path, @Param("parametros") String parametros, @Param("verbo") String verbo);

    @Query("FROM Servicio s JOIN FETCH s.roles rs JOIN FETCH rs.rol WHERE s.eliminado='N' and s.path=:path and s.parametros is null and s.verbo=:verbo")
    Servicio buscarPorNombreConRoles(@Param("path") String path, @Param("verbo") String verbo);

}
