package com.kruger.corp.repositorio;

import com.kruger.corp.modelo.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author cguevara94
 */
public interface IUsuarioRepo extends IRepositorioGenerico<Usuario, Long> {

    @Query("FROM Usuario u JOIN FETCH u.roles rs JOIN FETCH rs.rol WHERE u.eliminado='N' and u.nombre=:nombre")
    Usuario buscarPorNombreConRoles(@Param("nombre") String nombre);

}
