package com.kruger.corp.repositorio;

import com.kruger.corp.modelo.UsuarioRol;

/**
 *
 * @author cguevara94
 */
public interface IUsuarioRolRepo extends IRepositorioGenerico<UsuarioRol, Long> {

}
