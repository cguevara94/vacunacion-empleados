package com.kruger.corp.repositorio;

import com.kruger.corp.modelo.VacunacionEmpleado;

/**
 *
 * @author cguevara94
 */
public interface IVacunacionEmpleadoRepo extends IRepositorioGenerico<VacunacionEmpleado, Long> {

}
