package com.kruger.corp.seguridad;

import com.kruger.corp.modelo.Servicio;
import com.kruger.corp.servicio.impl.ApiServicioImpl;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl {

    @Autowired
    private ApiServicioImpl apiServicio;

    public boolean tieneAcceso(String path, HttpServletRequest request) {
        StringBuilder builder = null;

        Iterator<String> iterador = request.getParameterNames().asIterator();
        while (iterador.hasNext()) {
            String parametro = iterador.next();

            if (Objects.isNull(builder)) {
                builder = new StringBuilder();
            } else {
                builder.append(",");
            }

            builder.append(parametro);
        }

        String parametros = Objects.nonNull(builder) && builder.length() > 0 ? builder.toString() : null;

        Servicio servicio = apiServicio.buscarPorNombreConRoles(path, parametros, request.getMethod());
        List<String> rolesAutorizados = new ArrayList<>();

        servicio.getRoles().stream().forEach(item -> {
            rolesAutorizados.add(item.getRol().getNombre());
        });

        boolean rpta = false;

        Authentication usuarioLogueado = SecurityContextHolder.getContext().getAuthentication();

        for (GrantedAuthority auth : usuarioLogueado.getAuthorities()) {
            String rolUser = auth.getAuthority();

            for (String rolMet : rolesAutorizados) {
                if (rolUser.equalsIgnoreCase(rolMet)) {
                    rpta = true;
                    break;
                }
            }
        }

        return rpta;
    }
}
