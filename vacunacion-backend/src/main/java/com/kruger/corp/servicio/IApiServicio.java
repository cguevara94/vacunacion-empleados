/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kruger.corp.servicio;

import com.kruger.corp.modelo.Servicio;
import com.kruger.corp.modelo.Usuario;

/**
 *
 * @author cguevara94
 */
public interface IApiServicio extends ICRUD<Usuario, Long> {

    Servicio buscarPorNombreConRoles(String path, String parametros, String verbo);

}
