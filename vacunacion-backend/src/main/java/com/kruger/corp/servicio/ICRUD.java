package com.kruger.corp.servicio;

import com.kruger.corp.excepcion.RegistroNoEncontradoExcepcion;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author cguevara94
 */
public interface ICRUD<T, ID> {

    T registrar(T t);

    T modificar(T t);

    List<T> listar();

    T listarPorId(ID id);

    void eliminar(ID id) throws RegistroNoEncontradoExcepcion;

    Page<T> listarPageable(Pageable pageable);

}
