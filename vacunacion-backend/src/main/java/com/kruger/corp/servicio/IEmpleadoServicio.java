package com.kruger.corp.servicio;

import com.kruger.corp.excepcion.ActualizaRegistroExcepcion;
import com.kruger.corp.excepcion.InformacionNoValidaExcepcion;
import com.kruger.corp.modelo.Empleado;
import java.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author cguevara94
 */
public interface IEmpleadoServicio extends ICRUD<Empleado, Long> {

    Empleado registrarInformacionBasica(Empleado empleado);

    Empleado actualizarInformacionBasica(Empleado empleado) throws ActualizaRegistroExcepcion;

    Page<Empleado> buscarPorEstadoVacunacion(String estadoVacunacion, Pageable pageable);

    Page<Empleado> buscarPorVacuna(String nombreVacuna, Pageable pageable);

    Page<Empleado> buscarPorRangoFechaVacunacion(LocalDate fechaInicio, LocalDate fechaFin, Pageable pageable);

    Page<Empleado> buscarTodos(Pageable pageable);

    Empleado buscarPorId(Long id);
    
    Empleado actualizarInformacionAdicional(Empleado empleado) throws InformacionNoValidaExcepcion;

}
