package com.kruger.corp.servicio.impl;

import com.kruger.corp.modelo.Servicio;
import com.kruger.corp.modelo.Usuario;
import com.kruger.corp.repositorio.IRepositorioGenerico;
import com.kruger.corp.repositorio.IServicioRepo;
import com.kruger.corp.servicio.IApiServicio;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author cguevara94
 */
@Service
public class ApiServicioImpl extends CRUDImpl<Usuario, Long> implements IApiServicio {

    @Autowired
    private IServicioRepo repo;

    @Override
    protected IRepositorioGenerico<Usuario, Long> getRepo() {
        return repo;
    }

    @Override
    public Servicio buscarPorNombreConRoles(String path, String parametros, String verbo) {
        if (Objects.isNull(parametros)) {
            return repo.buscarPorNombreConRoles(path, verbo);
        }
        return repo.buscarPorNombreConRoles(path, parametros, verbo);
    }

}
