package com.kruger.corp.servicio.impl;

import com.kruger.corp.excepcion.RegistroNoEncontradoExcepcion;
import com.kruger.corp.modelo.EntidadBase;
import com.kruger.corp.repositorio.IRepositorioGenerico;
import com.kruger.corp.servicio.ICRUD;
import java.util.List;
import java.util.Objects;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author cguevara94
 * @param <T>
 * @param <ID>
 */
public abstract class CRUDImpl<T extends EntidadBase, ID> implements ICRUD<T, ID> {

    protected abstract IRepositorioGenerico<T, ID> getRepo();

    @Override
    public T registrar(T t) {
        return getRepo().save(t);
    }

    @Override
    public T modificar(T t) {
        return getRepo().save(t);
    }

    @Override
    public List<T> listar() {
        return getRepo().findAll();
    }

    @Override
    public T listarPorId(ID id) {
        return getRepo().findById(id).orElse(null);
    }

    @Override
    public void eliminar(ID id) throws RegistroNoEncontradoExcepcion {
        T resultado = listarPorId(id);
        if (Objects.isNull(resultado)) {
            throw new RegistroNoEncontradoExcepcion("No se encontro el registro que se intenta eliminar.");
        }
        resultado.marcarEliminado();
        modificar(resultado);
    }

    @Override
    public Page<T> listarPageable(Pageable pageable) {
        return getRepo().findAll(pageable);
    }

}
