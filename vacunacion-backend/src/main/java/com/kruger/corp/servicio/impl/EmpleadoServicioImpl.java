package com.kruger.corp.servicio.impl;

import com.kruger.corp.excepcion.ActualizaRegistroExcepcion;
import com.kruger.corp.excepcion.InformacionNoValidaExcepcion;
import com.kruger.corp.modelo.Empleado;
import com.kruger.corp.modelo.VacunacionEmpleado;
import com.kruger.corp.repositorio.IEmpleadoRepo;
import com.kruger.corp.repositorio.IRepositorioGenerico;
import com.kruger.corp.servicio.IEmpleadoServicio;
import com.kruger.corp.validacion.empleado.InformacionVacunacion;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cguevara94
 */
@Service
public class EmpleadoServicioImpl extends CRUDImpl<Empleado, Long> implements IEmpleadoServicio {

    @Autowired
    private IEmpleadoRepo repo;

    @Autowired
    private UsuarioServicioImpl usuarioServicio;

    @Autowired
    private VacunacionEmpleadoServicioImpl vacunacionEmpleadoServicioImpl;

    @Autowired
    private Validator validador;

    @Override
    protected IRepositorioGenerico<Empleado, Long> getRepo() {
        return repo;
    }

    @Override
    @Transactional
    public Empleado registrarInformacionBasica(Empleado empleado) {
        empleado = persistirEmpleado(empleado);

        usuarioServicio.crearUsuarioEmpleado(empleado);

        return empleado;
    }

    @Override
    public Empleado actualizarInformacionBasica(Empleado empleado) throws ActualizaRegistroExcepcion {
        if (Objects.isNull(empleado.getId())) {
            throw new ActualizaRegistroExcepcion("Empleado sin id para actualizar.");
        }

        return persistirEmpleado(empleado);
    }

    private Empleado persistirEmpleado(Empleado empleado) {
        if (empleado.tieneInfomacionAdicional() || empleado.tieneInfomacionVacunacion()) {
            empleado = new Empleado(empleado.getCedula(), empleado.getNombres(), empleado.getApellidos(), empleado.getEmail());
        }

        return repo.save(empleado);
    }

    @Override
    @Transactional
    public Empleado actualizarInformacionAdicional(Empleado empleado) throws InformacionNoValidaExcepcion {
        Long estadoVacunacion = null;
        if (Objects.nonNull(empleado.getEstadoVacunacion()) && Objects.nonNull(empleado.getEstadoVacunacion().getId())) {
            estadoVacunacion = empleado.getEstadoVacunacion().getId();
        }
        repo.actualizarInfoAdicional(empleado.getId(), empleado.getFechaNacimiento(),
                empleado.getDirDomicilio(), empleado.getNumMovil(), estadoVacunacion);

        if (empleado.esVacunado()) {
            registrarVacunacion(empleado);
        }

        return empleado;
    }

    private void registrarVacunacion(Empleado empleado) throws InformacionNoValidaExcepcion {

        Set<ConstraintViolation<Empleado>> errores = validador.validate(empleado, InformacionVacunacion.class);

        if (!errores.isEmpty()) {
            throw new InformacionNoValidaExcepcion(errores.iterator().next().getMessage());
        }

        for (VacunacionEmpleado vacunacion : empleado.getVacunacionEmpleado()) {

            vacunacion.setEmpleado(empleado);

            Set<ConstraintViolation<VacunacionEmpleado>> erroresVE = validador.validate(vacunacion);

            if (!erroresVE.isEmpty()) {
                throw new InformacionNoValidaExcepcion(erroresVE.iterator().next().getMessage());
            }

            if (vacunacion.getDosisRecibidas() < 1) {
                throw new InformacionNoValidaExcepcion("La dosis de vacunación no puede ser menor a 1");
            }
            if (vacunacion.getDosisRecibidas() > vacunacion.getVacuna().getDosisRequeridas()) {
                throw new InformacionNoValidaExcepcion("La dosis de vacunación no puede ser "
                        + "mayor a la dosis requeridas: " + vacunacion.getVacuna().getNombre()
                        + " dosis: " + vacunacion.getVacuna().getDosisRequeridas());
            }

            vacunacionEmpleadoServicioImpl.registrar(vacunacion);
        }
    }

    @Override
    public Page<Empleado> buscarPorEstadoVacunacion(String estadoVacunacion, Pageable pageable) {
        return repo.buscarPorEstadoVacunacion(estadoVacunacion, pageable);
    }

    @Override
    public Page<Empleado> buscarPorVacuna(String nombreVacuna, Pageable pageable) {
        return repo.buscarPorVacuna(nombreVacuna, pageable);
    }

    @Override
    public Page<Empleado> buscarPorRangoFechaVacunacion(LocalDate fechaInicio, LocalDate fechaFin, Pageable pageable) {
        return repo.buscarPorRangoFechaVacunacion(fechaInicio, fechaFin, pageable);
    }

    @Override
    public Page<Empleado> buscarTodos(Pageable pageable) {
        return repo.buscarTodos(pageable);
    }

    @Override
    public Empleado buscarPorId(Long id) {
        return repo.buscarPorId(id);
    }

}
