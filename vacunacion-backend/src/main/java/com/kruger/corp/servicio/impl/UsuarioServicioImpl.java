package com.kruger.corp.servicio.impl;

import com.kruger.corp.enumeracion.RolEnum;
import com.kruger.corp.modelo.Empleado;
import com.kruger.corp.modelo.Usuario;
import com.kruger.corp.modelo.UsuarioRol;
import com.kruger.corp.repositorio.IRepositorioGenerico;
import com.kruger.corp.repositorio.IRolRepo;
import com.kruger.corp.repositorio.IUsuarioRepo;
import com.kruger.corp.repositorio.IUsuarioRolRepo;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author cguevara94
 */
@Service
public class UsuarioServicioImpl extends CRUDImpl<Usuario, Long> implements UserDetailsService {

    @Autowired
    private IUsuarioRepo repo;

    @Autowired
    private IUsuarioRolRepo usuarioRolRepo;

    @Autowired
    private IRolRepo rolRepo;

    @Lazy
    @Autowired
    private BCryptPasswordEncoder bcrypt;

    @Override
    protected IRepositorioGenerico<Usuario, Long> getRepo() {
        return repo;
    }

    public Usuario crearUsuarioEmpleado(Empleado empleado) {
        Usuario usuarioEmpleado = new Usuario(empleado);

        usuarioEmpleado.setClave(bcrypt.encode(usuarioEmpleado.getClave()));
        UsuarioRol usuarioRol = new UsuarioRol(usuarioEmpleado, rolRepo.findByNombre(RolEnum.EMPLEADO.getNombreRol()));

        return usuarioRolRepo.save(usuarioRol).getUsuario();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Usuario usuario = repo.buscarPorNombreConRoles(username);

        if (usuario == null) {
            throw new UsernameNotFoundException(String.format("Usuario no existe", username));
        }

        List<GrantedAuthority> roles = new ArrayList<>();

        usuario.getRoles().forEach(rol -> {
            roles.add(new SimpleGrantedAuthority(rol.getRol().getNombre()));
        });

        UserDetails ud = new User(usuario.getNombre(), usuario.getClave(), !usuario.esEliminado(), true, true, true, roles);

        return ud;
    }

}
