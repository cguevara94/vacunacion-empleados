package com.kruger.corp.servicio.impl;

import com.kruger.corp.modelo.VacunacionEmpleado;
import com.kruger.corp.repositorio.IRepositorioGenerico;
import com.kruger.corp.repositorio.IVacunacionEmpleadoRepo;
import com.kruger.corp.servicio.IVacunacionEmpleadoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author cguevara94
 */
@Service
public class VacunacionEmpleadoServicioImpl extends CRUDImpl<VacunacionEmpleado, Long> implements IVacunacionEmpleadoServicio {

    @Autowired
    private IVacunacionEmpleadoRepo repo;

    @Override
    protected IRepositorioGenerico<VacunacionEmpleado, Long> getRepo() {
        return repo;
    }

}
