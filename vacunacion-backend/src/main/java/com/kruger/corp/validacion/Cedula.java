package com.kruger.corp.validacion;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 * @author cguevara94
 */
@Constraint(validatedBy = ValidadorCedulaEcuatoriana.class)
@Target({ElementType.FIELD})
@Retention(RUNTIME)
@Documented
public @interface Cedula {

    String message() default "Cédula invalida";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
