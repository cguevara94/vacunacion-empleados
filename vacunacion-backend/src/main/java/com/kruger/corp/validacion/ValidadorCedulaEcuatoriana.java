package com.kruger.corp.validacion;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author cguevara94
 */
public class ValidadorCedulaEcuatoriana implements ConstraintValidator<Cedula, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        
        if (Objects.isNull(value)) {
            return false;
        }
        Pattern pattern = Pattern.compile("^[0-9]{10,10}+$");
        Matcher matcher = pattern.matcher(value);
        boolean matches = matcher.matches();
        
        if (!matches) {
            return false;
        }
        String[] caracteres = value.split("");
        int provincia = Integer.valueOf(caracteres[0] + caracteres[1]);
        if (provincia < 1 || provincia > 24) {
            return false;
        }
        int tercerDigito = Integer.valueOf(caracteres[2]);
        if (tercerDigito > 6) {
            return false;
        }
        return validarDigitoVerificador(caracteres);
    }
    
    private boolean validarDigitoVerificador(String[] caracteres) {
        int digitoVerificador = 0;
        for (int i = 0 ; i < caracteres.length - 1 ; i++) {
            int digito = Integer.valueOf(caracteres[i]);
            if (i % 2 == 0) {
                digito *= 2;
                if (digito > 9) {
                    digito -= 9;
                }
            }
            digitoVerificador += digito;
        }
        digitoVerificador = (digitoVerificador - (digitoVerificador % 10) + 10) - digitoVerificador;
        return digitoVerificador == Integer.valueOf(caracteres[9]);
    }
    
}
