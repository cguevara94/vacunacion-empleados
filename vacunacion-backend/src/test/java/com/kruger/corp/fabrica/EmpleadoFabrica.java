package com.kruger.corp.fabrica;

import com.kruger.corp.modelo.Empleado;
import com.kruger.corp.modelo.EstadoVacunacion;
import com.kruger.corp.modelo.Vacuna;
import com.kruger.corp.modelo.VacunacionEmpleado;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;

/**
 *
 * @author cguevara94
 */
public final class EmpleadoFabrica {

    private EmpleadoFabrica() {
    }

    public static Empleado crearEmpleadoVacunado() {
        Empleado empleado = crearEmpleadoBase();
        empleado.setEstadoVacunacion(crearEstadoVacunacion("Vacunado"));

        VacunacionEmpleado vacunacionEmpleado = new VacunacionEmpleado(LocalDate.now(), 2, empleado, new Vacuna("Pfizer"));
        empleado.setVacunacionEmpleado(Arrays.asList(vacunacionEmpleado));

        return empleado;
    }

    public static Empleado crearEmpleadoNoVacunado() {
        Empleado empleado = crearEmpleadoBase();
        empleado.setEstadoVacunacion(crearEstadoVacunacion("No Vacunado"));

        return empleado;
    }

    public static Empleado crearEmpleadoBase() {
        Empleado empleado = new Empleado("0200562791", "Gabriel", "Guevara", "mimail@mail.com");

        return empleado;
    }

    public static Empleado crearEmpleadoInformacionAdicional() {
        Empleado empleado = crearEmpleadoBase();
        empleado.setDirDomicilio("Quito");
        empleado.setFechaNacimiento(LocalDate.of(1994, Month.MARCH, 10));
        empleado.setNumMovil("1234567890");

        return empleado;
    }

    private static EstadoVacunacion crearEstadoVacunacion(String estado) {
        return new EstadoVacunacion(estado);
    }

}
