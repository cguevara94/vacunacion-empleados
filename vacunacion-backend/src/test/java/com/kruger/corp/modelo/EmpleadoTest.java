/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kruger.corp.modelo;

import com.kruger.corp.fabrica.EmpleadoFabrica;
import com.kruger.corp.util.EjecutorValidacion;
import com.kruger.corp.validacion.empleado.InformacionAdicional;
import com.kruger.corp.validacion.empleado.InformacionBasica;
import com.kruger.corp.validacion.empleado.InformacionVacunacion;
import java.util.ArrayList;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author cguevara94
 */
public class EmpleadoTest {

    @Test
    public void testValidacionInformacionBasica() {
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(EmpleadoFabrica.crearEmpleadoNoVacunado(), InformacionBasica.class);
        
        assertTrue(errores.isEmpty(), "Errores en InformacionBasica");
    }
    
    @Test
    public void testValidacionInformacionBasicaErrorCedula() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoNoVacunado();
        
        empleado.setCedula("0200562790");
        
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionBasica.class);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en cedula");
    }
    
    @Test
    public void testValidacionInformacionBasicaNullCedula() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoNoVacunado();
        
        empleado.setCedula(null);
        
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionBasica.class);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en cedula");
    }
    
    @Test
    public void testValidacionInformacionBasicaNullCedulaLetras() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoNoVacunado();
        
        empleado.setCedula("123gfr7890");
        
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionBasica.class);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en cedula");
    }
    
    @Test
    public void testValidacionInformacionBasicaErrorEmail() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoNoVacunado();
        
        empleado.setEmail("qwerty");
                
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionBasica.class);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en email");
    }
    
    @Test
    public void testValidacionInformacionBasicaNullEmail() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoNoVacunado();
        
        empleado.setEmail(null);
                
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionBasica.class);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en email");
    }
    
    @Test
    public void testValidacionInformacionBasicaNullNombre() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoNoVacunado();
        
        empleado.setNombres(null);
                
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionBasica.class);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en nombre");
    }
    
    @Test
    public void testValidacionInformacionBasicaErrorNombre() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoNoVacunado();
        
        empleado.setNombres("Alt 12");
                
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionBasica.class);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en nombre");
    }
    
    @Test
    public void testValidacionInformacionBasicaNullApellido() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoNoVacunado();
        
        empleado.setApellidos(null);
                
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionBasica.class);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en apellido");
    }
    
    @Test
    public void testValidacionInformacionBasicaErrorApellido() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoNoVacunado();
        
        empleado.setNombres("Apellido 12");
                
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionBasica.class);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en apellido");
    }
    
    @Test
    public void testValidacionInformacionAdicionalVacia() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoNoVacunado();
                        
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionAdicional.class);
        
        assertTrue(errores.isEmpty(), "Informacion adicional vacio con errores");
    }
    
    @Test
    public void testValidacionInformacionAdicional() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoInformacionAdicional();
        
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionAdicional.class);
        
        assertTrue(errores.isEmpty(), "Informacion adicional con errores");
    }
    
    @Test
    public void testValidacionInformacionAdicionalErrorDirDomicilio() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoInformacionAdicional();
        
        empleado.setDirDomicilio("AC");
        
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionAdicional.class);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en direccion domicilio");
    }
    
    @Test
    public void testValidacionInformacionAdicionalErrorMovilLetras() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoInformacionAdicional();
        
        empleado.setNumMovil("qwertyuiop");
        
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionAdicional.class);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en movil");
    }
    
    @Test
    public void testValidacionInformacionAdicionalErrorMovilLongitud() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoInformacionAdicional();
        
        empleado.setNumMovil("123456789");
        
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionAdicional.class);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en movil");
    }
    
    @Test
    public void testValidacionInformacionVacunacionNull() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoNoVacunado();
        
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionVacunacion.class);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en vacunacion");
    }
    
    @Test
    public void testValidacionInformacionVacunacionVacia() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoNoVacunado();
        
        empleado.setVacunacionEmpleado(new ArrayList<>());
        
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionVacunacion.class);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en vacunacion");
    }
    
    @Test
    public void testValidacionInformacionVacunacion() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoVacunado();
        
        Set<ConstraintViolation<Empleado>> errores = EjecutorValidacion.validar(empleado, InformacionVacunacion.class);
        
        assertTrue(errores.isEmpty(), "Tiene errores en vacunacion");
    }
    
}
