package com.kruger.corp.modelo;

import com.kruger.corp.fabrica.EmpleadoFabrica;
import java.time.LocalDate;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;

/**
 *
 * @author cguevara94
 */
public class UsuarioTest {

    @Test
    public void testUsarioEmpleado() {
        Usuario usuario = new Usuario(EmpleadoFabrica.crearEmpleadoBase());

        Assertions.assertAll(
                () -> assertNotNull(usuario.getNombre(), "El nombre no debe ser null"),
                () -> assertNotNull(usuario.getClave(), "La clave no debe ser null"),
                () -> assertNotNull(usuario.getEmpleado(), "El empleado no debe ser null")
        );
    }

    @Test
    public void testObtenerUsarioEmpleado() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoBase();
        empleado.setNombres("Carlos Gabriel");
        empleado.setApellidos("Guevara Vera");

        Usuario usuario = new Usuario(empleado);

        LocalDate fechaHoy = LocalDate.now();

        String anio = String.valueOf(fechaHoy.getYear());
        anio = anio.substring(anio.length() - 2, anio.length());

        String usuarioEsperado = String.format("cgguevara%02d%02d%s",
                fechaHoy.getDayOfMonth(), fechaHoy.getMonthValue(), anio);

        Assertions.assertEquals(usuarioEsperado, usuario.getNombre());
    }

    @Test
    public void testObtenerUsarioEmpleadoUnNombreUnApellido() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoBase();
        empleado.setNombres("Carlos");
        empleado.setApellidos("Guevara");

        Usuario usuario = new Usuario(empleado);

        LocalDate fechaHoy = LocalDate.now();

        String anio = String.valueOf(fechaHoy.getYear());
        anio = anio.substring(anio.length() - 2, anio.length());

        String usuarioEsperado = String.format("cguevara%02d%02d%s",
                fechaHoy.getDayOfMonth(), fechaHoy.getMonthValue(), anio);

        Assertions.assertEquals(usuarioEsperado, usuario.getNombre());
    }

    @Test
    public void testClavePorDefecto() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoBase();

        Usuario usuario = new Usuario(empleado);

        Assertions.assertEquals(empleado.getCedula(), usuario.getClave());
    }

}
