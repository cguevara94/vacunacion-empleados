
package com.kruger.corp.modelo;

import com.kruger.corp.fabrica.EmpleadoFabrica;
import com.kruger.corp.util.EjecutorValidacion;
import java.time.LocalDate;
import java.util.Set;
import javax.validation.ConstraintViolation;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author cguevara94
 */
public class VacunacionEmpleadoTest {
    
    @Test
    public void testDatosValidos() {
        VacunacionEmpleado vacunacionEmpleado = crearVacunacionEmpleado();
        
        Set<ConstraintViolation<VacunacionEmpleado>> errores = EjecutorValidacion.validar(vacunacionEmpleado);
        
        assertTrue(errores.isEmpty(), "Tiene errores");
    }
    
    @Test
    public void testFechaNull() {
        VacunacionEmpleado vacunacionEmpleado = crearVacunacionEmpleado();
        
        vacunacionEmpleado.setFechaUltimaDosis(null);
        
        Set<ConstraintViolation<VacunacionEmpleado>> errores = EjecutorValidacion.validar(vacunacionEmpleado);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en fecha ultima dosis");
    }
    
    @Test
    public void testEmpleadoNull() {
        VacunacionEmpleado vacunacionEmpleado = crearVacunacionEmpleado();
        
        vacunacionEmpleado.setEmpleado(null);
        
        Set<ConstraintViolation<VacunacionEmpleado>> errores = EjecutorValidacion.validar(vacunacionEmpleado);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en empleado");
    }
    
    @Test
    public void testVacunaNull() {
        VacunacionEmpleado vacunacionEmpleado = crearVacunacionEmpleado();
        
        vacunacionEmpleado.setVacuna(null);
        
        Set<ConstraintViolation<VacunacionEmpleado>> errores = EjecutorValidacion.validar(vacunacionEmpleado);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en vacuna");
    }
    
    @Test
    public void testDosisRecibidasNull() {
        VacunacionEmpleado vacunacionEmpleado = crearVacunacionEmpleado();
        
        vacunacionEmpleado.setDosisRecibidas(null);
        
        Set<ConstraintViolation<VacunacionEmpleado>> errores = EjecutorValidacion.validar(vacunacionEmpleado);
        
        assertFalse(errores.isEmpty(), "Deberia tener errores en dosis recibidas");
    }
    
    private VacunacionEmpleado crearVacunacionEmpleado() {
        VacunacionEmpleado vacunacionEmpleado = 
                new VacunacionEmpleado(LocalDate.now(), 1, EmpleadoFabrica.crearEmpleadoVacunado(), new Vacuna("AstraZeneca"));
        
        return vacunacionEmpleado;
    }
    
}
