/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kruger.corp.servicio.impl;

import com.kruger.corp.enumeracion.EstadoVacunacionEnum;
import com.kruger.corp.excepcion.ActualizaRegistroExcepcion;
import com.kruger.corp.excepcion.InformacionNoValidaExcepcion;
import com.kruger.corp.excepcion.RegistroNoEncontradoExcepcion;
import com.kruger.corp.fabrica.EmpleadoFabrica;
import com.kruger.corp.modelo.Empleado;
import com.kruger.corp.modelo.EstadoVacunacion;
import com.kruger.corp.modelo.Vacuna;
import com.kruger.corp.modelo.VacunacionEmpleado;
import com.kruger.corp.repositorio.IEmpleadoRepo;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import javax.validation.Validator;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

/**
 *
 * @author cguevara94
 */
@SpringBootTest
public class EmpleadoServicioImplTest {

    @Mock
    private IEmpleadoRepo repo;
    @Mock
    private UsuarioServicioImpl usuarioServicio;
    @Mock
    private VacunacionEmpleadoServicioImpl vacunacionEmpleadoServicioImpl;
    @Mock
    private Validator validador;
    @InjectMocks
    private EmpleadoServicioImpl servicio;

    private Empleado empleadoActualizar;

    @BeforeEach
    public void setUp() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoBase();

        Mockito.when(repo.save(Mockito.any(Empleado.class)))
                .thenReturn(
                        empleado
                );

        empleadoActualizar = EmpleadoFabrica.crearEmpleadoBase();
        empleadoActualizar.setId(1L);

        Mockito.when(validador.validate(Mockito.any(), Mockito.any())).thenReturn(new HashSet<>());
        Mockito.when(validador.validate(Mockito.any())).thenReturn(new HashSet<>());
    }

    /**
     * Test of getRepo method, of class EmpleadoServicioImpl.
     */
    @Test
    public void testGetRepo() {
        assertNotNull(servicio.getRepo());
    }

    @Test
    public void testRegistrarInformacionBasicaNoAdicional() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoInformacionAdicional();

        Empleado empleadoRegistrado = servicio.registrarInformacionBasica(empleado);

        assertFalse(empleadoRegistrado.tieneInfomacionAdicional(), "Se ha persistido informacion adicional");
    }

    @Test
    public void testRegistrarInformacionBasicaNoVacunacion() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoVacunado();

        Empleado empleadoRegistrado = servicio.registrarInformacionBasica(empleado);

        assertFalse(empleadoRegistrado.tieneInfomacionVacunacion(), "Se ha persistido informacion de vacunación");
    }

    @Test
    public void testRegistrarInformacionBasicaPersistir() {
        servicio.registrarInformacionBasica(EmpleadoFabrica.crearEmpleadoNoVacunado());

        Mockito.verify(repo, Mockito.times(1)).save(Mockito.any(Empleado.class));
    }

    @Test
    public void testRegistraUsuario() {
        servicio.registrarInformacionBasica(EmpleadoFabrica.crearEmpleadoNoVacunado());

        Mockito.verify(usuarioServicio, Mockito.times(1)).crearUsuarioEmpleado(Mockito.any(Empleado.class));
    }

    @Test
    public void testEditarInformacionBasicaNoAdicional() throws ActualizaRegistroExcepcion {
        Empleado empleadoActualizado = servicio.actualizarInformacionBasica(empleadoActualizar);

        assertFalse(empleadoActualizado.tieneInfomacionAdicional(), "Se ha actualizado informacion adicional");
    }

    @Test
    public void testEditarInformacionBasicaNoVacunacion() throws ActualizaRegistroExcepcion {
        Empleado empleadoActualizado = servicio.actualizarInformacionBasica(empleadoActualizar);

        assertFalse(empleadoActualizado.tieneInfomacionVacunacion(), "Se ha actualizado informacion de vacunación");
    }

    @Test
    public void testEditarInformacionBasicaPersistir() throws ActualizaRegistroExcepcion {
        servicio.actualizarInformacionBasica(empleadoActualizar);

        Mockito.verify(repo, Mockito.times(1)).save(Mockito.any(Empleado.class));
    }

    @Test
    public void testEmpleadoSinIdActualizar() {
        Exception exception = assertThrows(ActualizaRegistroExcepcion.class, () -> {
            servicio.actualizarInformacionBasica(EmpleadoFabrica.crearEmpleadoNoVacunado());
        });

        assertEquals("Empleado sin id para actualizar.", exception.getMessage());
    }

    @Test
    public void testEmpleadoEliminarNoExiste() {
        Mockito.when(repo.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        Exception exception = assertThrows(RegistroNoEncontradoExcepcion.class, () -> {
            servicio.eliminar(empleadoActualizar.getId());
        });

        assertEquals("No se encontro el registro que se intenta eliminar.", exception.getMessage());
    }

    @Test
    public void testEmpleadoEliminar() throws RegistroNoEncontradoExcepcion {
        Mockito.when(repo.findById(Mockito.anyLong())).thenReturn(Optional.of(empleadoActualizar));

        servicio.eliminar(empleadoActualizar.getId());

        Mockito.verify(repo, Mockito.times(1)).save(Mockito.any(Empleado.class));
    }

    @Test
    public void testActualizarInfoAdicionalNullVacunar() throws InformacionNoValidaExcepcion {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoInformacionAdicional();
        empleado.setId(30L);

        Mockito.doNothing().when(repo).actualizarInfoAdicional(Mockito.anyLong(),
                Mockito.any(LocalDate.class), Mockito.anyString(), Mockito.anyString(), Mockito.any());

        servicio.actualizarInformacionAdicional(empleado);

        Mockito.verify(repo, Mockito.times(1)).actualizarInfoAdicional(Mockito.anyLong(),
                Mockito.any(LocalDate.class), Mockito.anyString(), Mockito.anyString(), Mockito.any());

        Mockito.verify(vacunacionEmpleadoServicioImpl, Mockito.times(0)).registrar(Mockito.any());
    }

    @Test
    public void testActualizarInfoAdicionalSinVacunar() throws InformacionNoValidaExcepcion {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoInformacionAdicional();
        empleado.setId(31L);
        empleado.setEstadoVacunacion(new EstadoVacunacion(EstadoVacunacionEnum.NO_VACUNADO.getDescripcion()));

        Mockito.doNothing().when(repo).actualizarInfoAdicional(Mockito.anyLong(),
                Mockito.any(LocalDate.class), Mockito.anyString(), Mockito.anyString(), Mockito.any());

        servicio.actualizarInformacionAdicional(empleado);

        Mockito.verify(repo, Mockito.times(1)).actualizarInfoAdicional(Mockito.anyLong(),
                Mockito.any(LocalDate.class), Mockito.anyString(), Mockito.anyString(), Mockito.any());

        Mockito.verify(vacunacionEmpleadoServicioImpl, Mockito.times(0)).registrar(Mockito.any());
    }

    @Test
    public void testActualizarInfoAdicionalVacunado() throws InformacionNoValidaExcepcion {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoInformacionAdicional();
        empleado.setId(32L);
        empleado.setEstadoVacunacion(new EstadoVacunacion(EstadoVacunacionEnum.VACUNADO.getDescripcion()));
        empleado.setVacunacionEmpleado(Arrays.asList(new VacunacionEmpleado(LocalDate.now(), 1, empleado, new Vacuna("Pfize", 2))));

        Mockito.doNothing().when(repo).actualizarInfoAdicional(Mockito.anyLong(),
                Mockito.any(LocalDate.class), Mockito.anyString(), Mockito.anyString(), Mockito.any());

        servicio.actualizarInformacionAdicional(empleado);

        Mockito.verify(repo, Mockito.times(1)).actualizarInfoAdicional(Mockito.anyLong(),
                Mockito.any(LocalDate.class), Mockito.anyString(), Mockito.anyString(), Mockito.any());

        Mockito.verify(vacunacionEmpleadoServicioImpl, Mockito.times(1)).registrar(Mockito.any());
    }

    @Test
    public void testActualizarInfoAdicionalVacunadoDosisMayorRequerida() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoInformacionAdicional();
        empleado.setId(32L);
        empleado.setEstadoVacunacion(new EstadoVacunacion(EstadoVacunacionEnum.VACUNADO.getDescripcion()));

        Vacuna vacuna = new Vacuna("Pfize", 2);
        empleado.setVacunacionEmpleado(Arrays.asList(new VacunacionEmpleado(LocalDate.now(), 3, empleado, vacuna)));

        Mockito.doNothing().when(repo).actualizarInfoAdicional(Mockito.anyLong(),
                Mockito.any(LocalDate.class), Mockito.anyString(), Mockito.anyString(), Mockito.any());

        InformacionNoValidaExcepcion resultado
                = assertThrows(InformacionNoValidaExcepcion.class, () -> servicio.actualizarInformacionAdicional(empleado));

        assertEquals("La dosis de vacunación no puede ser "
                + "mayor a la dosis requeridas: " + vacuna.getNombre()
                + " dosis: " + vacuna.getDosisRequeridas(), resultado.getMessage());

        Mockito.verify(repo, Mockito.times(1)).actualizarInfoAdicional(Mockito.anyLong(),
                Mockito.any(LocalDate.class), Mockito.anyString(), Mockito.anyString(), Mockito.any());

        Mockito.verify(vacunacionEmpleadoServicioImpl, Mockito.times(0)).registrar(Mockito.any());
    }

    @Test
    public void testActualizarInfoAdicionalVacunadoDosisCeroError() {
        Empleado empleado = EmpleadoFabrica.crearEmpleadoInformacionAdicional();
        empleado.setId(33L);
        empleado.setEstadoVacunacion(new EstadoVacunacion(EstadoVacunacionEnum.VACUNADO.getDescripcion()));

        Vacuna vacuna = new Vacuna("Pfize", 2);
        empleado.setVacunacionEmpleado(Arrays.asList(new VacunacionEmpleado(LocalDate.now(), 0, empleado, vacuna)));

        Mockito.doNothing().when(repo).actualizarInfoAdicional(Mockito.anyLong(),
                Mockito.any(LocalDate.class), Mockito.anyString(), Mockito.anyString(), Mockito.any());

        InformacionNoValidaExcepcion resultado
                = assertThrows(InformacionNoValidaExcepcion.class, () -> servicio.actualizarInformacionAdicional(empleado));

        assertEquals("La dosis de vacunación no puede ser menor a 1", resultado.getMessage());

        Mockito.verify(repo, Mockito.times(1)).actualizarInfoAdicional(Mockito.anyLong(),
                Mockito.any(LocalDate.class), Mockito.anyString(), Mockito.anyString(), Mockito.any());

        Mockito.verify(vacunacionEmpleadoServicioImpl, Mockito.times(0)).registrar(Mockito.any());
    }

}
