package com.kruger.corp.servicio.impl;

import com.kruger.corp.fabrica.EmpleadoFabrica;
import com.kruger.corp.modelo.Empleado;
import com.kruger.corp.modelo.Rol;
import com.kruger.corp.modelo.Usuario;
import com.kruger.corp.modelo.UsuarioRol;
import com.kruger.corp.repositorio.IRolRepo;
import com.kruger.corp.repositorio.IUsuarioRolRepo;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author cguevara94
 */
@SpringBootTest
public class UsuarioServicioImplTest {

    @Mock
    private IUsuarioRolRepo usuarioRolRepo;
    @Mock
    private IRolRepo rolRepo;
    @Mock
    private BCryptPasswordEncoder bcrypt;
    @InjectMocks
    private UsuarioServicioImpl servicio;

    @Test
    public void testPersistirUsuarioEmpleado() {
        Mockito.when(bcrypt.encode(Mockito.anyString())).thenReturn("!$@!44rwi87$%");

        Empleado empleado = EmpleadoFabrica.crearEmpleadoBase();

        Usuario usuario = new Usuario(empleado);

        Mockito.when(rolRepo.findByNombre(Mockito.anyString())).thenReturn(new Rol());

        Mockito.when(usuarioRolRepo.save(Mockito.any(UsuarioRol.class)))
                .thenReturn(new UsuarioRol(usuario, new Rol()));

        Usuario usuarioResultado = servicio.crearUsuarioEmpleado(empleado);

        assertTrue(usuarioResultado instanceof Usuario);

        assertEquals(usuario.getNombre(), usuarioResultado.getNombre());

        Mockito.verify(usuarioRolRepo, Mockito.times(1)).save(Mockito.any(UsuarioRol.class));
    }

}
