
package com.kruger.corp.util;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 *
 * @author cguevara94
 */
public final class EjecutorValidacion {

    private EjecutorValidacion() {
    }
    
    public static <T> Set<ConstraintViolation<T>> validar(T objeto, Class<?>... groups) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        
        Set<ConstraintViolation<T>> errores = validator.validate(objeto, groups);
        
        return errores;
    }
    
}
