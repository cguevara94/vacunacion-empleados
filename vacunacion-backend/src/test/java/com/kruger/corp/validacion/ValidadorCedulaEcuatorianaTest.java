
package com.kruger.corp.validacion;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;

/**
 *
 * @author cguevara94
 */
public class ValidadorCedulaEcuatorianaTest {

    @DisplayName("Valida null invalido")
    @Test
    public void testNullInvalido() {
        ValidadorCedulaEcuatoriana cedulaEcuatoriana = new ValidadorCedulaEcuatoriana();
        
        boolean resultado = cedulaEcuatoriana.isValid(null, null);
        
        assertFalse(resultado, "Cédula null no debe ser valida");
    }
    
    @DisplayName("Valida longitud invalida")
    @Test
    public void testLongitudValida() {
        ValidadorCedulaEcuatoriana cedulaEcuatoriana = new ValidadorCedulaEcuatoriana();
        
        boolean resultado = cedulaEcuatoriana.isValid("1234", null);
        
        assertFalse(resultado, "Una longitud de cédula diferente de 10 no puede ser valida");
    }
    
    @DisplayName("Valida provincia invalida")
    @Test
    public void testValidarProvincia() {
        ValidadorCedulaEcuatoriana cedulaEcuatoriana = new ValidadorCedulaEcuatoriana();
        
        boolean resultado = cedulaEcuatoriana.isValid("5612349087", null);
        
        assertFalse(resultado, "La cédula no puede ser valida cuando los dos primeros caracteres no corresponden a una provincia");
    }
    
    @DisplayName("Valida tercer dígito invalido")
    @Test
    public void testValidarTercerDigito() {
        ValidadorCedulaEcuatoriana cedulaEcuatoriana = new ValidadorCedulaEcuatoriana();
        
        boolean resultado = cedulaEcuatoriana.isValid("1172349087", null);
        
        assertFalse(resultado, "La cédula no puede tener como tercer dígito un número mayor a 6");
    }
    
    @DisplayName("Valida cedula valida")
    @Test
    public void testCedulaValida() {
        ValidadorCedulaEcuatoriana cedulaEcuatoriana = new ValidadorCedulaEcuatoriana();
        
        boolean resultado = cedulaEcuatoriana.isValid("0200562791", null);
        
        assertTrue(resultado, "La cédula no es valida");
    }
    
    @DisplayName("Valida cedula invalida")
    @Test
    public void testCedulaInvalida() {
        ValidadorCedulaEcuatoriana cedulaEcuatoriana = new ValidadorCedulaEcuatoriana();
        
        boolean resultado = cedulaEcuatoriana.isValid("0200562792", null);
        
        assertFalse(resultado, "La cédula es valida");
    }
    
}
